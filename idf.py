from math import log

# to see how many documents contain
# each word.
#
# @dictlist: a list of dictionaries,
#   each one containing the words of
#   a document.
def df(dictList):
    # will be returned as the result
    DFdict = dict()

    # swipes through the dictionaries of the corpus
    for dictionary in (dictList):
        # swipes through the words of a the document
        for word in (dictionary):
            # checks if it's a new word
            if word not in (DFdict):
                # default value
                DFdict[word] = 0
                # looks from the word in the whole corpus
                for _dictionary in (dictList):
                    # if a document is found with the word
                    if word in (_dictionary):
                        # adds to the dictionary
                        DFdict[word] += 1
    return DFdict

# to divide the value of DF
# by the number of documents.
#
# @dictList: a list of dictionaries,
#   each one containing the words of
#   a document.
#
# @n: the number of documents
def idf(dictList, n):
    # calculates DF
    IDFdict = df(dictList)
    
    # does the divition
    for item in (IDFdict):
        IDFdict[item] = log((n + 1) / IDFdict[item])

    return IDFdict