import glob
import unigram
import idf
import time

# scans the whole .\docs dir after any text file
docsList = glob.glob(".\\docs\\*.txt")

# a list for words in docs
contentList = list()

# key = docList -- value = contentList
docAndContentDict = dict()

# to fill up the docAndContentDict
for doc in (docsList):

    # prints the current file name
    print("Scanning ", doc)

    wl = unigram.wordCounter(unigram.wordSplitter(doc))
    contentList.append(wl)
    docAndContentDict[doc] = wl

print("\n\tCalculating...\n")

# key = word -- value = calculated IDF
idfDict = idf.idf(contentList, len(contentList))

while (True):
    # key = docName -- value = rank
    searchResault = dict()

    # the input query from the user
    query = input("\nEnter your query: ")

    if (query == "."):
        break

    start_time = time.time()

    # turns the query into a list of words
    queryList = unigram.inputSplitter(query)

    #queryList = ['sunt', 'aliquip'] # test

    for word in (queryList):
        for doc, wordDict in (docAndContentDict.items()):
            if word in wordDict:
                if doc not in (searchResault):
                    searchResault[doc] = (wordDict[word] * idfDict[word])
                else:
                    searchResault[doc] += (wordDict[word] * idfDict[word])


    sortedSearchResault = sorted(searchResault.items(), key = lambda kv: kv[1])
    sortedSearchResault.reverse()

    if (len(sortedSearchResault) == 0):
        print("\n\tNo resaults found! :(")

    else:
        i = 1
        for item in (sortedSearchResault):
            print(i, "- ", item)
            i += 1
        
        elapsed_time = time.time() - start_time
        print("\n\tFound", len(sortedSearchResault), "resaults")
        print("\tin", elapsed_time , "seconds")


#print(idf.idf(contentList,3))
#for item, value in (docAndContentDict.items()):
#    print('>>\t', item)
#    print(value)
#    #print(docAndContentDict[item])
#    print('\n')