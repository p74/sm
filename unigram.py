import string

# to count the frequency ratio of each word
# will return a dictionary
# words are keys
# frequency ratios are values
def unigram(wordDict):
    # d, which is the total number of the words
    delta = len(wordDict)

    # our final dictionary
    # words are keys
    # ratios are values
    ug = dict()

    # to calculate the ratio of a word's frequency
    for key in list(wordDict.keys()):
        ug[key] = wordDict[key] / delta * 100

    return ug

# to count the frequency of each word
# will return a dictionary
def wordCounter(wordsList):

    # will be returned as a dict with
    # words as keys and quantities as values
    wordDict = dict()

    # sweeps the list for each word
    for word in wordsList:
        # checks if the word is already in dictionary
        if word in wordDict:
            # increament by one as it's already there
            wordDict[word] = wordDict[word] + 1
        else:
            # value would be one, as it's a new word
            wordDict[word] = 1

    return wordDict


# reads a text file
# will return a list of each word
def wordSplitter(textAddress):
    # opens a text file
    #text = open(textAddress, "r")
    text = open(textAddress, errors='ignore')

    # will be returned as a list of seprated words
    words = list()

    for line in text:
        # to delete whitespaces
        line = line.strip()

        # to lower the uppercase chars
        line = line.lower()

        #to remove punctuation
        line = line.translate(line.maketrans("", "", string.punctuation))

        # to seprate each word and
        # add it to a list
        words.extend(line.split(" "))

    # do you really need a doc for this?!
    return words

# same as wordSplitter() but for an
# input string itself
def inputSplitter(inputStr):
    inputStr = inputStr.strip()
    inputStr = inputStr.lower()
    inputStr = inputStr.translate(inputStr.maketrans("", "", string.punctuation))
    return inputStr.split(" ")


# -----------------------------------------------------------
# test codes

# to read the file and pass it trough methods
# wordDict = unigram(wordCounter(wordSplitter("sample.text")))

# to print the final dictionary
# for key in list(wordDict.keys()):
    #print(key, ": ", wordDict[key], "%")
